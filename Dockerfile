FROM public.ecr.aws/docker/library/eclipse-temurin:21.0.3_9-jdk-alpine as build
WORKDIR /build
ADD . .
RUN --mount=type=cache,target=/root/.m2 ./mvnw -f pom.xml clean package -Dmaven.test.skip

FROM public.ecr.aws/docker/library/eclipse-temurin:21.0.3_9-jdk-alpine
ARG JAR_FILE=/build/target/*.jar
COPY --from=build $JAR_FILE /app/dtm.jar
ENTRYPOINT java -jar /app/dtm.jar