package com.cudatuda.docker_telegram_manager.bot;

import com.cudatuda.docker_telegram_manager.model.ContainerAction;

public enum CallbackAction {
    // this order determines container action buttons in tg bot
    INFO("info"),
    CONTAINER_START("start"),
    CONTAINER_PAUSE("pause"),
    CONTAINER_STOP("stop"),
    CONTAINER_RESTART("restart"),
    CONTAINER_REMOVE("remove"),
    CONTAINER_REFRESH("refresh data"),
    MENU("menu");

    public final String title;

    private CallbackAction(String title) {
        this.title = title;
    }

    public ContainerAction toContainerAction() {
        return switch (this) {
            case CONTAINER_RESTART ->  ContainerAction.RESTART;
            case CONTAINER_START -> ContainerAction.START;
            case CONTAINER_STOP -> ContainerAction.STOP;
            case CONTAINER_PAUSE -> ContainerAction.PAUSE;
            case CONTAINER_REMOVE -> ContainerAction.REMOVE;
            default -> null;
        };
    }
}
