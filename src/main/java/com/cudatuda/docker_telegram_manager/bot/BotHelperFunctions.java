package com.cudatuda.docker_telegram_manager.bot;

import com.cudatuda.docker_telegram_manager.model.ContainerAction;
import com.cudatuda.docker_telegram_manager.model.ContainerInfo;
import com.cudatuda.docker_telegram_manager.service.ContainerService;
import com.github.dockerjava.api.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardRow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class BotHelperFunctions {

    @Autowired
    ContainerService containerService;
    @Value("${telegram.callback.delimiter}")
    private String callbackDelimiter;
    private final Logger logger = LoggerFactory.getLogger(TgBot.class);

    public AnswerCallbackQuery answerCallbackQueryEmpty(String callbackQueryId) {
        return AnswerCallbackQuery
                .builder()
                .callbackQueryId(callbackQueryId)
                .build();
    }



    public AnswerCallbackQuery manipulateContainer(String callbackQueryId, String containerId, ContainerAction containerAction) {
        String alertText;
        try {
            alertText = containerService.manipulateContainer(containerId, containerAction);
        } catch (NotFoundException e) {
            logger.error("container " + containerId + " not found");
            alertText = "Container " + containerId + " not found";
        }
        return AnswerCallbackQuery
                .builder()
                .callbackQueryId(callbackQueryId)
                .text(alertText)
                .showAlert(true)
                .build();
    }

    public EditMessageText refreshContainerInfo(long messageId, long chatId, String containerId) {
        return EditMessageText
                .builder()
                .messageId(Math.toIntExact(messageId))
                .chatId(chatId)
                .text(getContainerInfoText(containerId))
                .parseMode("HTML")
                .replyMarkup(getContainerActions(containerId))
                .build();
    }



    public SendMessage getContainerList(Long chatId) {
        List<ContainerInfo> containers = containerService.getContainersSimple();
        List<InlineKeyboardRow> inlineKeyboardRows = new ArrayList<>();
        for (ContainerInfo container: containers) {

            InlineKeyboardButton inlineKeyboardButton = InlineKeyboardButton
                    .builder()
                    .text(container.getName())
                    .callbackData(CallbackAction.INFO.name() + callbackDelimiter + container.getId()
                    ).build();
            InlineKeyboardRow inlineKeyboardRow = new InlineKeyboardRow();
            inlineKeyboardRow.add(inlineKeyboardButton);
            inlineKeyboardRows.add(inlineKeyboardRow);
        }

        return SendMessage
                .builder()
                .chatId(chatId)
                .text("Containers list")
                .parseMode("HTML")
                .replyMarkup(InlineKeyboardMarkup
                        .builder()
                        .keyboard(inlineKeyboardRows)
                        . build()
                )
                .build();
    }

    public SendMessage getContainerInfo(long chatId, String containerId) {
        return SendMessage
                .builder()
                .chatId(chatId)
                .text(getContainerInfoText(containerId))
                .parseMode("HTML")
                .replyMarkup(getContainerActions(containerId)
                )
                .build();
    }

    private String getContainerInfoText(String containerId) {
        ContainerInfo ci = containerService.getContainerDetailedInfo(containerId);
        String text = String.format("""
                <b>Container info</b>
                name: <code>%s</code>
                image: <code>%s</code>
                status: <code>%s</code>
                """,
                ci.getName(),
                ci.getImage(),
                ci.getStatus()
        );

        if (ci.isRunning()) {
            text += String.format("""
                uptime: <code>%s</code>
                memory usage: <code>%s%%</code>
                cpu usage: <code>%s%%</code>
                """,
                    ci.getUptime(),
                    containerService.getContainerMemoryUsage(containerId),
                    containerService.getContainerCpuUsage(containerId)
            );
        }

        text += String.format("""
                fetch time: <code>%s</code>
                """,
                ci.getFetchTime()
        );

        return text;
    }

    private InlineKeyboardMarkup getContainerActions(String containerId) {

        List<CallbackAction> containerActions = new ArrayList<>();
        for (CallbackAction callbackAction: CallbackAction.values()) {
            if (callbackAction.name().startsWith("CONTAINER")) {
                containerActions.add(callbackAction);
            }
        }

        List<InlineKeyboardRow> inlineKeyboardRows = new ArrayList<>();
        for (CallbackAction containerAction: containerActions) {

            InlineKeyboardButton inlineKeyboardButton = InlineKeyboardButton
                    .builder()
                    .text(containerAction.title)
                    .callbackData(containerAction.name() + callbackDelimiter + containerId).build();
            InlineKeyboardRow inlineKeyboardRow = new InlineKeyboardRow();
            inlineKeyboardRow.add(inlineKeyboardButton);
            inlineKeyboardRows.add(inlineKeyboardRow);
        }

        return InlineKeyboardMarkup
                        .builder()
                        .keyboard(inlineKeyboardRows)
                        .build();
    }
}
