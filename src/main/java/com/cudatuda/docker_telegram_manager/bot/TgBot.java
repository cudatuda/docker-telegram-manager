package com.cudatuda.docker_telegram_manager.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.longpolling.interfaces.LongPollingUpdateConsumer;
import org.telegram.telegrambots.longpolling.starter.SpringLongPollingBot;
import org.telegram.telegrambots.longpolling.util.LongPollingSingleThreadUpdateConsumer;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.TelegramClient;
import org.telegram.telegrambots.client.okhttp.OkHttpTelegramClient;

@Component
public class TgBot implements SpringLongPollingBot, LongPollingSingleThreadUpdateConsumer {

    @Value("${TG_BOT_TOKEN}")
    String botToken;
    private final TelegramClient telegramClient;
    private final Logger logger = LoggerFactory.getLogger(TgBot.class);
    @Autowired
    BotHelperFunctions botHelperFunctions;
    @Value("${telegram.callback.delimiter}")
    private String callbackDelimiter;

    public TgBot(@Value("${TG_BOT_TOKEN}") String botToken) {
        telegramClient = new OkHttpTelegramClient(botToken);
        logger.info("Bot initialized");
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public LongPollingUpdateConsumer getUpdatesConsumer() {
        return this;
    }

    @Override
    public void consume(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            handleMessage(update);
        }
        else if (update.hasCallbackQuery()) {
            handleCallback(update);
        }
    }

    private void handleMessage(Update update) {
        Long chatId = update.getMessage().getChatId();
        String messageText = update.getMessage().getText();
        logger.info(String.format("[INCOMING MESSAGE] from: %s | text: %s", chatId, messageText));

        switch (messageText) {
            case "/start":
                execute(botHelperFunctions.getContainerList(chatId));

                break;
        }
    }

    private void handleCallback(Update update) {
        CallbackQuery callbackQuery = update.getCallbackQuery();
        Long chatId = callbackQuery.getFrom().getId();
        String callbackData = callbackQuery.getData();
        logger.info(String.format("[CALLBACK] from: %s | data: %s", chatId, callbackData));

        CallbackAction callbackAction = CallbackAction.valueOf(callbackData.split(callbackDelimiter)[0]);

        switch (callbackAction) {
            case INFO -> {
                String containerId;
                try {
                    containerId = callbackData.split(callbackDelimiter)[1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    logger.error("could not extract containerId from callback data");
                    return;
                }
                execute(botHelperFunctions.getContainerInfo(chatId, containerId));
            }

            case CONTAINER_REFRESH ->  {
                String containerId;
                try {
                    containerId = callbackData.split(callbackDelimiter)[1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    logger.error("could not extract containerId from callback data");
                    return;
                }
                execute(botHelperFunctions.refreshContainerInfo(callbackQuery.getMessage().getMessageId(), chatId, containerId));
            }

            case CONTAINER_RESTART, CONTAINER_START, CONTAINER_STOP, CONTAINER_PAUSE, CONTAINER_REMOVE -> {
                String containerId;
                try {
                    containerId = callbackData.split(callbackDelimiter)[1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    logger.error("could not extract containerId from callback data");
                    return;
                }
                execute(botHelperFunctions.manipulateContainer(callbackQuery.getId(), containerId, callbackAction.toContainerAction()));
                execute(botHelperFunctions.refreshContainerInfo(callbackQuery.getMessage().getMessageId(), chatId, containerId));
            }

        }
        execute(botHelperFunctions.answerCallbackQueryEmpty(callbackQuery.getId()));
    }


    private void execute(EditMessageText editMessageText) {
        try {
            logger.info(String.format("[EDIT MESSAGE] to: %s | text: %s", editMessageText.getChatId(), editMessageText.getText()));
            telegramClient.execute(editMessageText);
        } catch (TelegramApiException e) {
            logger.error(e.getMessage(), e);
        }
    }
    private void execute(SendMessage sendMessage) {
        try {
            logger.info(String.format("[OUTGOING MESSAGE] to: %s | text: %s", sendMessage.getChatId(), sendMessage.getText()));
            telegramClient.execute(sendMessage);
        } catch (TelegramApiException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void execute(AnswerCallbackQuery answerCallbackQuery) {
        try {
            logger.debug(String.format("[ANSWER CALLBACK] id: %s", answerCallbackQuery.getCallbackQueryId()));
            telegramClient.execute(answerCallbackQuery);
        } catch (TelegramApiException e) {
            logger.error(e.getMessage(), e);
        }
    }



}
