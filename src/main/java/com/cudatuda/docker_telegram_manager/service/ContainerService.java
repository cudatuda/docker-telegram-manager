package com.cudatuda.docker_telegram_manager.service;

import com.cudatuda.docker_telegram_manager.model.ContainerAction;
import com.cudatuda.docker_telegram_manager.model.ContainerInfo;
import com.cudatuda.docker_telegram_manager.model.ContainerStatus;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.command.InspectImageResponse;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.exception.ConflictException;
import com.github.dockerjava.api.exception.NotFoundException;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Statistics;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.core.InvocationBuilder;
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient;
import com.github.dockerjava.transport.DockerHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class ContainerService {

    @Autowired
    DateTimeService dateTimeService;

    private final Logger logger = LoggerFactory.getLogger(ContainerService.class);

    private final DockerClient dockerClient;

    public ContainerService() {
        DockerClientConfig dockerConfig = DefaultDockerClientConfig.createDefaultConfigBuilder().build();
        DockerHttpClient dockerHttpClient = new ApacheDockerHttpClient.Builder()
                .dockerHost(dockerConfig.getDockerHost())
                .sslConfig(dockerConfig.getSSLConfig())
                .maxConnections(100)
                .connectionTimeout(Duration.ofSeconds(30))
                .responseTimeout(Duration.ofSeconds(45))
                .build();
        dockerClient = DockerClientImpl.getInstance(dockerConfig, dockerHttpClient);

    }

    public String manipulateContainer(String containerId, ContainerAction containerAction) {
        switch (containerAction) {
            case START -> {
                try {
                    dockerClient.startContainerCmd(containerId).exec();
                } catch (ConflictException e) {
                    logger.warn(e.getMessage());
                    dockerClient.unpauseContainerCmd(containerId).exec();
                }
                return String.format("Container %s started successfully", containerId);
            }
            case STOP -> {
                dockerClient.stopContainerCmd(containerId).exec();
                return String.format("Container %s stopped successfully", containerId);
            }
            case RESTART -> {
                dockerClient.restartContainerCmd(containerId).exec();
                return String.format("Container %s restarted successfully", containerId);
            }
            case PAUSE -> {
                dockerClient.pauseContainerCmd(containerId).exec();
                return String.format("Container %s paused successfully", containerId);
            }
            case REMOVE -> {
                try {
                    dockerClient.removeContainerCmd(containerId).exec();
                } catch (ConflictException e) {
                    logger.warn(e.getMessage());
                    return String.format("Container %s is running. Stop it before remove", containerId);
                }
                return String.format("Container %s removed successfully", containerId);

            }
        }
        return "Error while container manipulation. Check server logs.";
    }


    public double getContainerMemoryUsage(String containerId) {
        double memoryUsage = 0.0;
        try {
            InvocationBuilder.AsyncResultCallback<Statistics> callback = new InvocationBuilder.AsyncResultCallback<>();
            dockerClient.statsCmd(containerId).withNoStream(true).exec(callback);
            Statistics stats = callback.awaitResult();

            // https://docs.docker.com/engine/api/v1.40/#tag/Container/operation/ContainerStats
            long usedMemory = stats.getMemoryStats().getUsage()
                    - (
                    (stats.getMemoryStats().getStats().getCache() != null)
                            ? stats.getMemoryStats().getStats().getCache() : 0);
            long availableMemory = stats.getMemoryStats().getLimit();
            memoryUsage = ((double) usedMemory / availableMemory) * 100;

            callback.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } catch (NullPointerException n) {
            logger.error(n.getMessage(), n);
            memoryUsage = 0.0;
        }

        return Double.parseDouble(String.format("%.2f", memoryUsage));
    }


    public double getContainerCpuUsage(String containerId) {
        double cpuUsage = 0.0;

        try {
            InvocationBuilder.AsyncResultCallback<Statistics> callback = new InvocationBuilder.AsyncResultCallback<>();
            dockerClient.statsCmd(containerId).withNoStream(true).exec(callback);
            Statistics stats = callback.awaitResult();

            // https://docs.docker.com/engine/api/v1.40/#tag/Container/operation/ContainerStats

            long cpuDelta = stats.getCpuStats().getCpuUsage().getTotalUsage() - stats.getPreCpuStats().getCpuUsage().getTotalUsage();
            long systemCpuDelta = stats.getCpuStats().getSystemCpuUsage() - stats.getPreCpuStats().getSystemCpuUsage();
            long numberCpus = stats.getCpuStats().getOnlineCpus();
            if (cpuDelta > 0 && systemCpuDelta > 0) {
                cpuUsage = ((double) cpuDelta / systemCpuDelta) * numberCpus * 100;
            }

            callback.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } catch (NullPointerException n) {
            logger.error(n.getMessage(), n);
            cpuUsage = 0.0;
        }

        return Double.parseDouble(String.format("%.2f", cpuUsage));
    }

    /**
     * Gets containers.
     *
     * @return the list of containers
     */
    public List<Container> getContainers() {
        return dockerClient.listContainersCmd().withShowAll(true).exec();
    }

    public List<ContainerInfo> getContainersSimple() {
        List<ContainerInfo> result = new ArrayList<>();
        for (Container container: getContainers()) {
            result.add(
                    new ContainerInfo(
                            container.getId(),
                            container.getNames()[0].replaceFirst("/", ""),
                            dateTimeService.getNowHoursAndSeconds()
                    )
            );
        }
        // sort list alphabetically
        result.sort(new Comparator<ContainerInfo>() {
            public int compare(ContainerInfo c1, ContainerInfo c2) {
                return c1.getName().compareTo(c2.getName());
            }
        });
        return result;
    }
    private String getContainerUptime(String containerId) {
        Long time = new Date().getTime();
        InspectContainerResponse containerInfo = dockerClient.inspectContainerCmd(containerId).exec();

        String containerStartedAtStr = Objects.requireNonNull(containerInfo.getState().getStartedAt());
        String containerFinishedAtStr = Objects.requireNonNull(containerInfo.getState().getFinishedAt());

        // remove milliseconds from the end of string
        LocalDateTime containerStartedAt = LocalDateTime.parse(containerStartedAtStr.replace("Z", "").split("\\.")[0]);
        LocalDateTime containerFinishedAt = LocalDateTime.parse(containerFinishedAtStr.replace("Z", "").split("\\.")[0]);
        logger.debug(String.format("Container startedAt: %s, finishedAt: %s", containerFinishedAt, containerFinishedAt));
        // check if container is running
        if (Boolean.TRUE.equals(containerInfo.getState().getRunning())) {
            return dateTimeService.getTimeDurationBetween(containerStartedAt, LocalDateTime.now(ZoneOffset.UTC));
        }

        return "0";

    }


    public ContainerInfo getContainerDetailedInfo(String containerId) {
        InspectContainerResponse containerInspect = dockerClient.inspectContainerCmd(containerId).exec();
        InspectImageResponse imageInfo = dockerClient.inspectImageCmd(containerInspect.getImageId()).exec();
        boolean containerIsRunning = Boolean.TRUE.equals(containerInspect.getState().getRunning());

        ContainerInfo containerInfo = new ContainerInfo(
                containerInspect.getId(),
                containerInspect.getName().replaceFirst("/", ""),
                String.join(", ", Objects.requireNonNull(imageInfo.getRepoTags())),
                ContainerStatus.valueOf(containerInspect.getState().getStatus()),
                containerIsRunning,
                dateTimeService.getNowHoursAndSeconds()
        );

        // add runtime stats if container is running
        if (containerIsRunning) {
            containerInfo.setUptime(getContainerUptime(containerId));
            containerInfo.setCpuUsage(getContainerCpuUsage(containerId));
            containerInfo.setMemoryUsage(getContainerMemoryUsage(containerId));
        }
        logger.debug(containerInfo.toString());
        return containerInfo;
    }

}
