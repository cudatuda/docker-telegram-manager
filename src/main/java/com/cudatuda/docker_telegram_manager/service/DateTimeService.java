package com.cudatuda.docker_telegram_manager.service;

import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

@Service
public class DateTimeService {
    public String getTimeDurationBetween(LocalDateTime fromDateTime, LocalDateTime toDateTime) {
        Duration duration = Duration.between(fromDateTime, toDateTime);
        if (duration.toDays() > 2) {
            return duration.toDays() + " dys";
        } else if (duration.toHours() > 2) {
            return duration.toHours() + " hrs";
        } else if (duration.toMinutes() >= 1) {
            return duration.toMinutes() + " min";
        } else if (duration.toSeconds() >= 1) {
            return duration.toSeconds() + " sec";
        }
        return "err";
    }

    public String getNowHoursAndSeconds() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(new Date());
    }
}
