package com.cudatuda.docker_telegram_manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerTelegramManagerApplication {

	private final Logger logger = LoggerFactory.getLogger(DockerTelegramManagerApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DockerTelegramManagerApplication.class, args);
	}

}
