package com.cudatuda.docker_telegram_manager.model;

public enum ContainerAction {
    START,
    STOP,
    RESTART,
    PAUSE,
    REMOVE
}
