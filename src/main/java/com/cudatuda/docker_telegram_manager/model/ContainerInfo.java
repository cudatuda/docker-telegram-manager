package com.cudatuda.docker_telegram_manager.model;

public class ContainerInfo {

    private String id;
    private String name;
    private String image;
    private ContainerStatus status;
    private String uptime;
    private double cpuUsage;
    private double memoryUsage;
    private boolean isRunning;
    private String fetchTime;

    public String getFetchTime() {
        return fetchTime;
    }

    public void setFetchTime(String fetchTime) {
        this.fetchTime = fetchTime;
    }
    public double getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(double cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public double getMemoryUsage() {
        return memoryUsage;
    }

    public void setMemoryUsage(double memoryUsage) {
        this.memoryUsage = memoryUsage;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public String getUptime() {
        return uptime;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime;
    }
    public ContainerStatus getStatus() {
        return status;
    }

    public void setStatus(ContainerStatus status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public ContainerInfo(String id, String name, String image, ContainerStatus state, boolean isRunning, String fetchTime) {
        // Get first 12 characters for simplified manipulation
        //https://nickjanetakis.com/blog/docker-tip-52-referencing-containers-and-images-by-their-short-ids
        this.id = id.substring(0, 12);

        this.name = name;
        this.image = image;
        this.status = state;
        this.isRunning = isRunning;
        this.fetchTime = fetchTime;
    }


    public ContainerInfo(String id, String name, String fetchTime) {
        // Get first 12 characters for simplified manipulation
        //https://nickjanetakis.com/blog/docker-tip-52-referencing-containers-and-images-by-their-short-ids
        this.id = id.substring(0, 12);
        this.name = name;
        this.fetchTime = fetchTime;
    }

    @Override
    public String toString() {
        return "ContainerInfo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", status=" + status +
                ", uptime='" + uptime + '\'' +
                ", cpuUsage=" + cpuUsage +
                ", memoryUsage=" + memoryUsage +
                ", isRunning=" + isRunning +
                ", fetchTime='" + fetchTime + '\'' +
                '}';
    }
}
