package com.cudatuda.docker_telegram_manager.model;

// https://docs.docker.com/reference/cli/docker/container/ls/#status
public enum ContainerStatus {
    created,
    running,
    paused,
    restarting,
    exited,
    removing,
    dead
}
